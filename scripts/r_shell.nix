{
  pkgs2111 ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/21.11.tar.gz";
    sha256 = "sha256:162dywda2dvfj1248afxc45kcrg83appjd0nmdb541hl7rnncf02";
  }) {}
}:
let
  myRPackages = with pkgs2111.rPackages; [
    tidyverse
    gridExtra
    patchwork
    viridis
    stringr
    rjson
    rlist
  ];
  myR = pkgs2111.rWrapper.override { packages = myRPackages; };

in
  pkgs2111.mkShell {
    buildInputs = [
      myR
    ];
  }