library(gridExtra)
library(tidyverse)
library(patchwork)
library(viridis)
library(stringr)
library(rlist)
library(rjson)

args<-commandArgs(TRUE)

if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} 


#################################### GLOBAL VARIABLES #################################### 
filename <- args[1]
full_path <-  paste("input/", filename,sep='')
folder <- str_replace(filename, ".json", "")
myData <- fromJSON(file=full_path)
df <- as.data.frame(myData)
dc_list <- df$DCs


######################################### FIGURE 2 #####################################################
generate_figure_2_partial <- function(file,title)
 {
  df <- read_delim(skip=1,file, col_names = c("timestamp","ghi"), delim = ";", show_col_types = FALSE)
  df$date <- as.POSIXct(df$timestamp, format="%Y-%m-%d", tz="UTC")
  avg_df <- aggregate(df[, 2], list(df$date), mean) # column 2 is the GHI
  avg_df$ut_date <- as.POSIXct(avg_df$Group.1, format="%Y-%m-%d", tz="UTC")

  plot <- ggplot(data=avg_df, aes(x=ut_date,y=ghi)) +
    geom_area(colour="#5ec962",fill="#5ec962") +
    scale_x_datetime(labels = date_format("%Y-%m-%d %H"),
                    date_breaks = "1 month",date_labels =  "%b")+
    theme_bw() +     
    labs(x="Time in months",
        y="Mean Daily GHI (Wh/m\u00B2)")+

  
    ggtitle(title)  
    return(plot)
 }
 
key <- paste('solar_irradiance_dc_file_path', dc_list[1], sep = '.')
    

if (length(dc_list) == 1)
{

    key <- paste('solar_irradiance_dc_file_path', dc_list[1], sep = '.')
    solar_input <-as.character(df[key][1,1])
    final_plot <- generate_figure_2_partial(solar_input, dc_list[1])


} else {

    key <- paste('solar_irradiance_dc_file_path', dc_list[1], sep = '.')
    solar_input <-as.character(df[key][1,1])

    final_plot <-ggplot() + theme_void()
    for ( x in 1:length(dc_list)) 
    {
        key <- paste('solar_irradiance_dc_file_path', dc_list[x], sep = '.')
        solar_input <-as.character(df[key][1,1])
        final_plot <-final_plot /  generate_figure_2_partial(solar_input, dc_list[x])
    }
}

 output  <- paste("results/", folder, '/figure_2.pdf',sep='')

 ggsave(
   output,
   plot = final_plot,
   device = 'pdf',
   path = './',
   scale = 1,
   width = 5,
   height = length(dc_list)*3,
   dpi = 300,
   units = c("in"),
   limitsize = FALSE
 )


######################################### FIGURE 3 #####################################################

file  <- paste("results/", folder, '/figure_3_data.csv',sep='')
df <- read_delim(file,  delim = ";", show_col_types = FALSE)
df_bat = filter(df, type == "battery")
df_pv = filter(df, type == "pv")

max_first  <- max(df_bat$value)   # Specify max of first y axis
max_second <- max(df_pv$value) # Specify max of second y axis
min_first  <- min(df_bat$value)   # Specify min of first y axis
min_second <- min(df_pv$value) # Specify min of second y axis
# scale and shift variables calculated based on desired mins and maxes
scale = (max_second - min_second)/(max_first - min_first)
shift = min_first - min_second


df_scale <- df %>% mutate(scaled_value=ifelse(type=="battery",value,value/scale))
final_plot <- ggplot(df_scale,aes(x=dc, y = scaled_value,fill= type)) + 
  theme_bw()+
  geom_col(position="dodge") + 
  scale_fill_viridis_d(name ="Type:",labels = c("Battery", "Photovoltaic Panel"))  +
  scale_y_continuous(sec.axis = sec_axis(~ . *scale, name = "PV Area (m\u00B2)"))+
  labs(x= "Data center", y="Battery Capacity (kWh)")+
  
  theme(legend.position="bottom",,legend.text = element_text(size = 19),legend.title = element_text(size = 19),
        axis.text.x = element_text(angle = 90,size = 18),axis.text.y = element_text(size = 14),
        axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
        plot.title = element_text(size = 16,face="bold",hjust = 0.5))

output_file <- paste("results/", folder, '/figure_3.pdf',sep='')
ggsave(
  output_file,
  plot = final_plot,
  device = 'pdf',
  path = './',
  scale = 1,
  width = 10,
  height = 6,
  dpi = 300,
  units = c("in"),
  limitsize = FALSE
)


######################################### FIGURE 4 #####################################################

generate_figure_4_partial <- function(file,dc_name,title, legend){
  df <- read_delim(file,  delim = ";", show_col_types = FALSE)
  df_dc = filter(df, dc == dc_name)
  plot <- ggplot(df_dc,aes(x=day, y = value,fill= type)) + 
    theme_bw()+
    geom_area() + 
    ggtitle( title)+
    scale_fill_viridis_d(name ="Energy source:",labels = c("Discharged from battery", "Photovoltaic", "Electrical grid"))    +
    labs(x= "Day", y="Energy Ratio")

  if (legend) 
  {
    plot <- plot +  theme(legend.position="bottom", legend.box = "vertical",legend.text = element_text(size = 19),legend.title = element_text(size = 19),
          axis.text.x = element_text(angle = 90,size = 18),axis.text.y = element_text(size = 14),
          axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
          plot.title = element_text(size = 16,face="bold",hjust = 0.5))

  }
  else {
    plot <- plot + theme(legend.position="none", 
        axis.text.x = element_text(angle = 90,size = 18),axis.text.y = element_text(size = 14),
        axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
        plot.title = element_text(size = 16,face="bold",hjust = 0.5))
  }
  
  return(plot)

}


file <- paste("results/", folder, '/figure_4_data.csv',sep='')

if (length(dc_list) == 1)
{
    final_plot <- generate_figure_3_partial(file,dc_list[1],dc_list[1],TRUE) 

} else {

    final_plot <-ggplot() + theme_void()
    
    for ( x in 2:length(dc_list)-1) 
    {
        final_plot <- final_plot / generate_figure_4_partial(file,dc_list[x],dc_list[x],FALSE)
    }
    
    final_plot <- final_plot / generate_figure_4_partial(file,dc_list[length(dc_list)],dc_list[length(dc_list)],TRUE)

}

output_file <- paste("results/", folder, '/figure_4.pdf',sep='')
ggsave(
  output_file,
  plot = final_plot,
  device = 'pdf',
  path  = './',
  scale = 1,
  width = 10,
  height = 21,
  dpi = 300,
  units = c("in"),
  limitsize = FALSE
)



######################################### FIGURE 5 #####################################################


 
 get_max_value_1_week <- function(file) {
   df <- read_delim(file,  delim = ";", show_col_types = FALSE)
   max_pv = max(df$pv, na.rm = TRUE)
   max_grid = max(df$grid, na.rm = TRUE)
   max_bat = max(df$bat, na.rm = TRUE)
   max_pow = max(df$power, na.rm = TRUE)
   maxs = c(max_pv,max_bat,max_pow,max_grid)
   max_y = max(maxs,na.rm = TRUE)   
   return (max_y)
 }

 generate_figure_5_partial <- function(file, dc_name, i, j, title, legend, max_y) {
            
   df <- read_delim(file,  delim = ";", show_col_types = FALSE)
   df_dc = filter(df, dc == dc_name)

   df_pv = df_dc[c("time","pv")]
   df_pv$type <- 'Photovoltaic'
   names(df_pv)[2] <- 'value'  
   df_grid = df_dc[c("time","grid")]
   df_grid$type <- 'Electrical grid'
   names(df_grid)[2] <- 'value'      
   df_bat = df_dc[c("time","bat")]
   df_bat$type <- 'Discharged from battery'
   names(df_bat)[2] <- 'value'   
   df_power = df_dc[c("time","power")]
   df_power$type <- 'DC Power demand'
   names(df_power)[2] <- 'value'
   new_df <- bind_rows(df_pv, df_grid, df_power, df_bat)

   plot <- ggplot(new_df,aes(x=time, y = value,color= type,linetype =type, shape = type)) + 
     theme_bw()+
     geom_line() + 
     geom_point() +
     xlim(i, j)+     
     ylim(0,max_y)+
     labs(x= "Time (hour)", y="Power (W)")+
     ggtitle(title)+
     scale_color_manual(values = c("#31688e", "#440154", "#fde725","#21918c"))+
     labs(color  = "Source", linetype = "Source", shape = "Source")

  if (legend) {
    plot <- plot +  theme(legend.position="bottom", legend.box = "vertical",legend.text = element_text(size = 19),legend.title = element_text(size = 19),
           axis.text.x = element_text(angle = 90,size = 18),axis.text.y = element_text(size = 14),
           axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
           plot.title = element_text(size = 16,face="bold",hjust = 0.5))
  }
  else {
    plot <- plot + theme(legend.position="none", 
           axis.text.x = element_text(,size = 18),axis.text.y = element_text(size = 14),
           axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
           plot.title = element_text(size = 16,face="bold",hjust = 0.5))
  }
   
   return(plot)
 }

file  <- paste("results/", folder, '/figure_4_data.csv',sep='')
max_y <- get_max_value_1_week(file)

if (length(dc_list) == 1)
{
    final_plot <- generate_figure_5_partial(file,dc_list[1],1,24,dc_list[1],TRUE,max_y)

} else {
    final_plot <-ggplot() + theme_void()
    for ( x in 2:length(dc_list)-1) 
    {
        final_plot <- final_plot / generate_figure_5_partial(file,dc_list[x],1,24,dc_list[x],FALSE,max_y)
    }    
    final_plot <- final_plot / generate_figure_5_partial(file,dc_list[length(dc_list)],1,24,dc_list[length(dc_list)],TRUE,max_y)

}


output  <- paste("results/", folder, '/figure_5.pdf',sep='')

ggsave(
  output,
  plot = final_plot,
  device = 'pdf',
  path = './',
  scale = 1,
  width = 12,
  height = 23,
  dpi = 300,
  units = c("in"),
  limitsize = FALSE
)
