import pytest
from low_carbon_cloud import *
from util import Util
import json

def test_arguments_from_cli(mocker):
    """Test whether arguments from the command line are set up correctly."""
    mocker.patch(
        "sys.argv",
        [
            "low_carbon_cloud.py",
            "--input_file",
            "input/example.json",
             "--use_gurobi",
             "--only_write_lp_file",
        ],
    )

    app = LowCarbonCloudLP('example_name')    
    assert app.args.input_file == "input/example.json"
    assert app.args.use_gurobi == True
    assert app.args.only_write_lp_file == True

    ### If the flags are not passed, their value should be False:
    mocker.patch(
        "sys.argv",
        [
            "low_carbon_cloud.py",
            "--input_file",
            "input/example.json",
        ])

    app = LowCarbonCloudLP('example_name')    
    assert app.args.input_file == "input/example.json"
    assert app.args.use_gurobi == False
    assert app.args.only_write_lp_file == False

def test_no_input_file_args_from_cli(mocker):
    """Test when not passing the input file argument."""
    mocker.patch(
        "sys.argv",
        [
            "low_carbon_cloud.py"            
        ],
    )
    with pytest.raises(RuntimeError,match=r"Need to pass a input file!"):
        LowCarbonCloudLP('example_name')    


def test_input_file_not_exists(mocker):
    """Test when passing an input file that does not exists"""
    mocker.patch(
        "sys.argv",
        [
            "low_carbon_cloud.py",
            "--input_file",
            "input/file_that_does_not_exists.json",
        ],
    )

    with pytest.raises(RuntimeError,match=r"Input file must exist!"):
        LowCarbonCloudLP('example_name')    

def test_valid_json_input_file():
    json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
    json_data_processed = json.loads(json_data)
    result = Util.validate_json_input(json_data_processed)    
    assert result == True

def test_invalid_json_no_key_DCs(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the DCs list in the input!"):
        json_data =  ' { "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    

def test_invalid_json_no_DCs(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the at least one DC in the DC list of the input!"):
        json_data =  ' { "DCs": [], "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    

def test_invalid_json_no_key_pIdleDCs(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the pIdleDC of the DCs in the input!"):
        json_data =  ' { "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    


def test_invalid_json_pIdleDCs_not_same_as_DCs_list(): 
    with pytest.raises(RuntimeError,match=r"All DCs must have a pIdle value!"):
        json_data =  ' { "DCs": ["DC_A"],"pIdleDC": {"DC_B":2250400},  "PUE": {"DC_A":1.1 },"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)



def test_invalid_json_no_key_PUE(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the PUE of the DCs in the input!"):
        json_data =  ' { "DCs": ["DC_A"],  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    


def test_invalid_json_PUE_not_same_as_DCs_list(): 
    with pytest.raises(RuntimeError,match=r"All DCs must have a PUE value!"):
        json_data =  ' { "DCs": ["DC_A"],"pIdleDC": {"DC_A":2250400},  "PUE": {"DC_B":1.1 },"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)



def test_invalid_json_no_key_C(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter C of the DCs in the input!"):
        json_data =  ' { "DCs": ["DC_A"],  "pIdleDC": {"DC_A":2250400}, "PUE": {"DC_A":1.1 },"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    


def test_invalid_json_C_not_same_as_DCs_list(): 
    with pytest.raises(RuntimeError,match=r"All DCs must have a C value!"):
        json_data =  ' { "DCs": ["DC_A"],"pIdleDC": {"DC_A":2250400},  "PUE": {"DC_A":1.1 },"C":{"DC_B":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)

def test_invalid_json_no_key_pNetIntraDC(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter pNetIntraDC of the DCs in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    


def test_invalid_json_pNetIntraDC_not_same_as_DCs_list(): 
    with pytest.raises(RuntimeError,match=r"All DCs must have a pNetIntraDC value!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_B":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)


def test_invalid_json_no_key_length_k(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter length_k in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    

def test_invalid_json_no_key_workload_file(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter workload_file in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    

def test_invalid_json_no_key_solar_irradiance_dc_file_path(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter solar_irradiance_dc_file_path of the DCs in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv", "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    


def test_invalid_json_solar_irradiance_dc_file_path_not_same_as_DCs_list(): 
    with pytest.raises(RuntimeError,match=r"All DCs must have a solar_irradiance_dc_file_path value!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_B":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)

def test_invalid_json_no_key_gridco2(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter grid_co2 of the DCs in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    


def test_invalid_json_grid_co2_not_same_as_DCs_list(): 
    with pytest.raises(RuntimeError,match=r"All DCs must have a grid_co2 value!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_B":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)
        


def test_invalid_json_no_key_pv_co2(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter pv_co2 of the DCs in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0},  "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    


def test_invalid_json_pv_co2_not_same_as_DCs_list(): 
    with pytest.raises(RuntimeError,match=r"All DCs must have a pv_co2 value!"):
        json_data = '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_B":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)




def test_invalid_json_no_key_bat_co2(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter bat_co2 in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0},  "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    

def test_invalid_json_no_key_eta_ch(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter eta_ch in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0,  "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    

def test_invalid_json_no_key_eta_dch(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter eta_dch in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    

def test_invalid_json_no_key_eta_pv(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter eta_pv in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "pCore": 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)   

def test_invalid_json_no_key_pCore(): 
    with pytest.raises(RuntimeError,match=r"Necessary to pass the parameter pCore in the input!"):
        json_data =  '{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0 }'
        json_data_processed = json.loads(json_data)
        Util.validate_json_input(json_data_processed)    



#'{ "DCs": ["DC_A"],  "PUE": {"DC_A":1.1 },  "pIdleDC": {"DC_A":2250400},"C":{"DC_A":278400},"pNetIntraDC":   {"DC_A":149760}, "length_k": 1,"workload_file": "file.csv","solar_irradiance_dc_file_path":{ "DC_A":"file.csv" }, "grid_co2" :  {"DC_A":1.0}, "pv_co2":   { "DC_A":1.0}, "bat_co2" : 1.0, "eta_ch": 1.0, "eta_dch": 1.0, "eta_pv" : 1.0, "pCore": 1.0 }'

