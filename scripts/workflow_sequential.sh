#!/bin/bash
echo '===================================================='
echo ' Starting the experiments workflow...'
echo '===================================================='
bash scripts/run_all_sequential.sh
bash scripts/generate_plots.sh
echo '===================================================='
echo 'Validating the results...'
echo '===================================================='
python3 scripts/test_output.py --input_file input/files_to_test.json
echo '===================================================='
echo 'All done! You may also validate the results at the results directory'
echo '===================================================='
echo '===================================================='
echo ' Experiments workflow completed !'
echo '===================================================='