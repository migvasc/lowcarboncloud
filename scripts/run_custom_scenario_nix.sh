#!/bin/bash
mkdir -p results

echo '===================================================='
echo 'Running the custom experiment...'
echo '===================================================='

## Running the experiments
nix-shell scripts/py_shell.nix --run "python3 scripts/low_carbon_cloud.py --input_file input/$1" 

echo '===================================================='
echo 'Extracting the data from the custom experiment...'
echo '===================================================='
## Extract the data
nix-shell scripts/py_shell.nix --run "python3 scripts/extract_data_figures.py --input_file input/$1"


# Generating  the plots for each experiment
echo '===================================================='
echo 'Generating the plots...'
echo '===================================================='


## Generate the plots
nix-shell scripts/r_shell.nix --run "Rscript scripts/custom_plots.r $1"

echo '===================================================='
echo 'All done! You may check the results at the results directory'
echo '===================================================='
