from util import Util
import argparse


def build_metrics_data_dictionary(path):
    """ Creates a hash with the metrics DCU, GEC, CO2 savings
    for each DC

    Parameters
    ----------
    path : str
        The path of the csv input file

    Returns
    -------
    hash
        A hash containing the metrics and their values
    """

    dictionary = {}
    with open(path, encoding="UTF-8") as summary_file:    
        cursor = 0
        for line_number, line in enumerate(summary_file):
            columns = line.split(';')
            dc = columns[0]
            metric = columns[1]
            value = columns[2].replace('\n','')
            key = dc+'_'+metric
            dictionary[key] = value
    return dictionary


def extract_data_table_vii(dict_pv_bat_grid,orderDCs):
    """ Export the values of the metrics GEC and CO2 savings
    to a external csv file

    Parameters
    ----------
    dict_pv_bat_grid : hash
        The hash that contains the metrics and their values
    
    orderDCs : list
        A list of the DCs names sorted by a criteria (here it was the carbon emissions)

    """
    result_file = open('results/table_vii_data.csv', 'w')    
    result_file.write('location;GEC;CO2_savings\n')

    for dc in orderDCs:
        result_file.write(f'{dc};{ dict_pv_bat_grid[dc+"_GEC"] };{dict_pv_bat_grid[dc+"_CO2_savings"] }\n')

    result_file.close()

def extract_data_table_vi(dict_grid,dict_pv_bat,dict_pv_bat_grid,orderDCs):
    """ Export the values of the metric DCU to a external csv file

    Parameters
    ----------
    dict_pv_bat_grid : hash
        The hash that contains the metric and its value for each DC
    
    orderDCs : list
        A list of the DCs names sorted by a criteria (here it was the carbon emissions)

    """
    result_file = open('results/table_vi_data.csv', 'w')    
    result_file.write('location;grid;pv_bat;pv_bat_grid\n')
    for dc in orderDCs:
        result_file.write(f'{dc};{ dict_grid[dc+"_DCU"] };{dict_pv_bat[dc+"_DCU"]};{dict_pv_bat_grid[dc+"_DCU"] }\n')
    result_file.close()

### DCs sorted by decreasing order of grid carbon emissions
orderDCs = [ "DC_JOHANNESBURG","DC_PUNE","DC_CANBERRA","DC_DUBAI","DC_SINGAPORE","DC_SEOUL","DC_VIRGINIA","DC_SP","DC_PARIS"]

dict_pv_bat_grid = build_metrics_data_dictionary("results/2021/metrics.csv")
dict_pv_bat = build_metrics_data_dictionary("results/only_pv_bat/metrics.csv")
dict_grid = build_metrics_data_dictionary("results/only_grid/metrics.csv")


extract_data_table_vi(dict_grid,dict_pv_bat,dict_pv_bat_grid,orderDCs)
extract_data_table_vii(dict_pv_bat_grid,orderDCs)