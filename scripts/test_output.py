import argparse
import json


def load_sizing(file):
    dict_sizing = {}
    with open(file) as sizing_file:    
        cursor = 0
        for line_number, line in enumerate(sizing_file):            
            if (line_number > 0):                    
                columns = line.split(';')
                dc = columns[0]
                type = columns[1]
                value = round(float(columns[2].replace('\n','')),2)
                dict_sizing[dc+'_'+type] = value
    return dict_sizing

def load_emissions(file):
    dict_emissions = {}
    with open(file) as emissions_file:    
        cursor = 0
        for line_number, line in enumerate(emissions_file):            
            if (line_number > 0):                    
                columns = line.split(';')
                type = columns[0]
                value = round(float(columns[1].replace('\n','')),2)
                dict_emissions[type] = value
    return dict_emissions

def compare(expected_dict,test_dict):
    for key in expected_dict:
        if not (key in test_dict):  
            return False
        delta = abs(expected_dict[key] -test_dict[key] )
        if delta > 1: # greater than square 1 meter or 1 gram CO2 eq difference
            print(key,expected_dict[key],test_dict[key])
            return False
    return True


def compare_results(json_input):
    expected_dict = json_input['expected']
    test_dict = json_input['to_test']
    dict_sizing_test = load_sizing(test_dict['figure_3_data'])

    dict_sizing_expected = load_sizing(expected_dict['figure_3_data'])
    valid = compare(dict_sizing_expected,dict_sizing_test)
    if valid: 
        print('Sizing results validated!! the value obtained is equal to the expected result.')

    dict_emissions_expected = load_emissions(expected_dict['table_v_data'])
    dict_emissions_test = load_emissions(test_dict['table_v_data'])
    valid = compare(dict_emissions_expected,dict_emissions_test)
    if valid: 
        print('Total emissions results validated!! the value obtained is equal to the expected result.')



parser = argparse.ArgumentParser(description='Testing of the outputs')
parser.add_argument('--input_file', metavar = 'input_file', type=str,
                    help = 'filename with the inputs')

res = parser.parse_args()  
input_file = res.input_file
with open(input_file, 'r') as f:
    json_input = json.load(f)
    compare_results(json_input)