import argparse
import os
from util import Util


def compute_CO2_savings(dc,dict_variables,inputs):
    """ Compute the CO2 savings metric for each DC.
    More details of the metric in: 10.1109/TSUSC.2017.2701883

    Parameters
    ----------
    dc : str
        The DC name
    dict_variables : hash
        A hash that contains the variables of the LP and their computed values
    inputs : hash
        A hash that contains the inputs used for the experiment

    Returns
    -------
    float
        The CO2 savings for the DC
    """

    dc_total_consumption_baseline = 0
    dc_total_grid_power = 0
    dc_total_green_power = 0        
    for timeslot in range(1,len(inputs['timeslots'])):
        dc_total_consumption_baseline+= ((dict_variables['w__'+dc+'_'+str(timeslot)] * inputs['pCore'] + inputs['pNetIntraDC'][dc]+ inputs['pIdleDC'][dc])*inputs['PUE'][dc])/1000 # convert from Wh to kwh
        dc_total_grid_power+= dict_variables['Pgrid__'+dc+'_'+str(timeslot)]
        dc_total_green_power+= inputs['irradiance'][dc][timeslot] * inputs['eta_pv']* float(dict_variables['A_'+dc])/1000 ## convert from Wh to kwh
             
    dc_co2_current = dc_total_grid_power*inputs['grid_co2'][dc] + dc_total_green_power*inputs['pv_co2'][dc] + dict_variables['Bat_'+dc]*inputs['bat_co2']
    dc_co2_baseline = dc_total_consumption_baseline * inputs['grid_co2'][dc]
    return  round((1 - dc_co2_current/dc_co2_baseline)*100,2)


def compute_GEC(dc,inputs,dict_variables):
    """ Compute the Green Energy Coefficient (GEC) metric for each DC.
    More details of the metric in: 10.1109/TSUSC.2017.2701883

    Parameters
    ----------
    dc : str
        The DC name
    dict_variables : hash
        A hash that contains the variables of the LP and their computed values
    inputs : hash
        A hash that contains the inputs used for the experiment

    Returns
    -------
    float
        The GEC for the DC
    """
    dc_total_consumption = 0
    dc_total_green_power = 0
    for timeslot in range(1,len(inputs['timeslots'] )):
        dc_total_consumption+= ((dict_variables['w__'+dc+'_'+str(timeslot)] *inputs['pCore']  + inputs['pNetIntraDC'][dc]+ inputs['pIdleDC'] [dc])*inputs['PUE'] [dc]) # convert from Wh to kwh
        dc_total_green_power+= inputs['irradiance'][dc][timeslot] * inputs['eta_pv'] * float(dict_variables['A_'+dc]) # convert from Wh to kwh
                   
    return  round((dc_total_green_power/dc_total_consumption),2)


def compute_DCU(dc,inputs,dict_variables):
    """ Compute the Data Center Utilization (DCU) metric for each DC.
    More details of the metric in: 10.1109/TSUSC.2017.2701883

    Parameters
    ----------
    dc : str
        The DC name
    dict_variables : hash
        A hash that contains the variables of the LP and their computed values
    inputs : hash
        A hash that contains the inputs used for the experiment

    Returns
    -------
    float
        The DCU for the DC
    """
    dc_workload_received = 0
    dc_cumulative_max_capacity = (len(inputs['timeslots'])-1) * inputs['C'][dc]    
    for timeslot in range(1,len(inputs['timeslots'])):
        dc_workload_received+= dict_variables['w__'+dc+'_'+str(timeslot)]

    return  round((dc_workload_received/dc_cumulative_max_capacity)*100,2)


def extract_metrics(inputs,variables,output_path):
    """ Export the CO2 Savings, GEC and DCU metrics to a csv file.
    More details of these metrics in: 10.1109/TSUSC.2017.2701883

    Parameters
    ----------
    inputs : hash
        A hash that contains the inputs used for the experiment
    variables : hash
        A hash that contains the variables of the LP and their computed values
    output_path : str
        The path where the file will be written.
    """

    result_file = open(output_path, 'w')    
    result_file.write('dc;type;value\n')
    for dc in inputs['DCs']:
        dc_name = dc        
        gec = compute_GEC(dc,inputs,variables)
        co2_savings = compute_CO2_savings(dc,variables,inputs)
        dcu = compute_DCU(dc,inputs,variables)
        result_file.write(f'{dc_name};GEC;{gec}\n')
        result_file.write(f'{dc_name};CO2_savings;{co2_savings}\n')    
        result_file.write(f'{dc_name};DCU;{dcu}\n')    
    result_file.close()

def extract_figure_3_data(inputs,variables,output_path):
    """ Export the data for generating Figure 3 (the sizing values )

    Parameters
    ----------
    inputs : hash
        A hash that contains the inputs used for the experiment
    variables : hash
        A hash that contains the variables of the LP and their computed values
    output_path : str
        The path where the file will be written.
    """

    result_file = open(output_path, 'w')    
    result_file.write('dc;type;value\n')
    for dc in inputs['DCs']:
        dc_name = dc
        result_file.write(f'{dc_name};battery;{variables["Bat_"+ dc]}\n')
        result_file.write(f'{dc_name};pv;{variables["A_"+ dc]}\n')    
    result_file.close()


def extract_figure_4_data(inputs,dict_variables,output_path ):    

    """ Export the data for generating Figure 4 (the ratio of used energy type per day )

    Parameters
    ----------
    inputs : hash
        A hash that contains the inputs used for the experiment
    variables : hash
        A hash that contains the variables of the LP and their computed values
    output_path : str
        The path where the file will be written.
    """
    result_file = open(output_path, 'w')    
    result_file.write('day;dc;type;value\n')
    
    dcs_daily_power = {}
    dcs_daily_grid_power = {}
    dcs_daily_green_power = {}
    dcs_daily_Pdch = {}
    cont = 1
    
    dcs_green_ratio ={}
    dcs_bat_ratio ={}
    dcs_grid_ratio ={}
    
    for d in inputs['DCs'] :        
        
        dcs_daily_power[d] = {}
        dcs_daily_grid_power[d] = {}
        dcs_daily_green_power[d] = {}
        dcs_daily_Pdch[d] = {}
        
        
        dcs_green_ratio[d] = []
        dcs_bat_ratio[d]   = []
        dcs_grid_ratio[d]  = []
    
        hour = 1
        day  = 1
        
        for timeslot in inputs['timeslots'] :                                
        
            if day not in dcs_daily_power[d]:
                dcs_daily_power[d][day] = 0.0
            if day not in dcs_daily_grid_power[d]:
                dcs_daily_grid_power[d][day] = 0.0
            
            if day not in  dcs_daily_green_power[d]:
                dcs_daily_green_power[d][day] =0.0
                
                
            if day not in  dcs_daily_Pdch[d]:
                dcs_daily_Pdch[d][day] =0.0
                                                                    
            power = (dict_variables['w__'+d+'_'+str(timeslot)] * inputs['pCore'] + inputs['pIdleDC'][d] + inputs['pNetIntraDC'][d] )*inputs['PUE'][d]                        
            green = inputs['irradiance'][d][timeslot] * inputs['eta_pv']* float(dict_variables['A_'+d])             


            # These values are multiplied by 1000 to convert from kW to W
            grid_power = dict_variables['Pgrid__'+d+'_'+str(timeslot)] * 1000.0
            Pdch  = dict_variables['Pdch__'+d+'_'+str(timeslot)] * 1000.0
            Pch  = dict_variables['Pdch__'+d+'_'+str(timeslot)] * 1000.0

            power = (dict_variables['w__'+d+'_'+str(timeslot)] * inputs['pCore'] + inputs['pIdleDC'][d] + inputs['pNetIntraDC'][d] )*inputs['PUE'][d]
            grid_power = dict_variables['Pgrid__'+d+'_'+str(timeslot)] * 1000.0
            green = inputs['irradiance'][d][timeslot] * inputs['eta_pv']* float(dict_variables['A_'+d])             
            Pdch  = dict_variables['Pdch__'+d+'_'+str(timeslot)] * 1000.0
            Pch  = dict_variables['Pch__'+d+'_'+str(timeslot)] * 1000.0

            bat_k =0
            bat_next_k =0
            if (timeslot+1) < 8761:                
                bat_k =dict_variables['B__'+d+'_'+str(timeslot)]
                bat_next_k = dict_variables['B__'+d+'_'+str(timeslot+1)]

            # If charge and discharge happens at the same time, we can compute a alternative solution that reaches the same level of energy and            
            if (Pdch > 1.0 and Pch > 1.0):
                delta = bat_next_k - bat_k
                
                if delta > 0 : 
                    Pch = delta/.85  
                    Pdch = 0
                elif delta < 0:
                    Pch = 0
                    Pdch = delta * .85  
       
            dcs_daily_power[d][day] += power                                                    
            dcs_daily_grid_power[d][day] += grid_power                              
            dcs_daily_green_power[d][day] +=  green      
            dcs_daily_Pdch[d][day] += Pdch 
            
            hour+=1            
            if hour >24:
                grid_ratio = dcs_daily_grid_power[d][day]/dcs_daily_power[d][day]
                bat_ratio  = dcs_daily_Pdch[d][day]/ dcs_daily_power[d][day]
                if bat_ratio > 1: 
                    bat_ratio = 1

                green_ratio = 1  - grid_ratio - bat_ratio

                dcs_green_ratio[d].append(green_ratio)
                dcs_bat_ratio[d] .append(bat_ratio)
                dcs_grid_ratio[d].append(grid_ratio)
                
                day+=1
                hour = 1

                
            
            cont+=1

    for d in inputs['DCs']:
        dc_name = d
        day = 1
        for bat_ratio in dcs_bat_ratio[d]:
            result_file.write(f'{day};{dc_name};battery;{bat_ratio}\n')
            day+=1        
        day = 1
        for grid_ratio in dcs_grid_ratio[d]:
            result_file.write(f'{day};{dc_name};grid;{grid_ratio}\n')
            day+=1
        day = 1
        for green_ratio in dcs_green_ratio[d]:
            result_file.write(f'{day};{dc_name};green;{green_ratio}\n')
            day+=1
    result_file.close()

            

def extract_figure_5_data(inputs,dict_variables,output_path ):    
    """ Export the data for generating Figure 5 (the source of used energy per hour)

    Parameters
    ----------
    inputs : hash
        A hash that contains the inputs used for the experiment
    variables : hash
        A hash that contains the variables of the LP and their computed values
    output_path : str
        The path where the file will be written.
    """
    result_file = open(output_path, 'w')    
    result_file.write('time;dc;power;grid;bat;pv\n')
    for d in inputs['DCs']:                        
        for timeslot in range (1,168):                                                                    
            power = (dict_variables['w__'+d+'_'+str(timeslot)] * inputs['pCore']  + inputs['pIdleDC'][d] + inputs['pNetIntraDC'][d] )*inputs['PUE'][d]            
            grid_power = dict_variables['Pgrid__'+d+'_'+str(timeslot)] * 1000.0
            green = inputs['irradiance'][d][timeslot] * inputs['eta_pv']* float(dict_variables['A_'+d])             
            Pdch  = dict_variables['Pdch__'+d+'_'+str(timeslot)] * 1000.0                         
            result_file.write(f'{timeslot};{d};{power};{grid_power};{Pdch};{green}\n')    
    result_file.close()

def extract_data(inputs, variables,file_name):
    """ Export the data from the LP solution that will be used for producing
    figures and tables

    Parameters
    ----------
    inputs : hash
        A hash that contains the inputs used for the experiment
    variables : hash
        A hash that contains the variables of the LP and their computed values
    filename : str
        The path where the results will be written.
    """

    output_path = f'results/{file_name}/'
    extract_figure_3_data(inputs,variables,output_path+'figure_3_data.csv')
    extract_figure_4_data(inputs, variables, output_path+'figure_4_data.csv' )
    extract_figure_5_data(inputs,variables,output_path+'figure_5_data.csv' )
    extract_metrics(inputs,variables,output_path+'metrics.csv' )

parser = argparse.ArgumentParser(description='Data extractor for the Low-carbon cloud sizing tool')
parser.add_argument('--input_file', metavar = 'input_file', type=str,
                    help = 'filename with the inputs')

res = parser.parse_args()  
input_file = res.input_file
inputs = Util.load_inputs(input_file)
file_name = input_file
folder_name = file_name.replace('.json','').replace('input/', '')
variables = Util.load_variables(f'results/{folder_name}/solution.csv')

extract_data(inputs,variables,folder_name)
