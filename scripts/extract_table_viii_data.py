### Compute and export the Mean Absolute Percentage Error 
# value for each DC, regarding the sizing of PV panels and batteries
def create_dict_vars(path):
    var_dict= {}
    with open(path, encoding="UTF-8") as dict_file:    
        cursor = 0
        for line_number, line in enumerate(dict_file):
            row = line.split(';')
            var_name = row[0]
            var_value = float(row[1].replace('\n',''))
            if "Bat_DC" in var_name: 
                var_dict[var_name]=var_value
            if "A_DC" in var_name: 
                var_dict[var_name]=var_value  
    return var_dict

years = ['2020','2019','2018']
path = "results/"
base_year = '2021'
dicts_years = {}

full_path = path+base_year+'/solution.csv'
dicts_years[base_year] = create_dict_vars(full_path)

DCs = ["DC_JOHANNESBURG", "DC_PUNE","DC_CANBERRA", "DC_DUBAI", "DC_SINGAPORE", "DC_SEOUL", "DC_VIRGINIA",  "DC_SP", "DC_PARIS"   ]

for year in years:
    full_path = path+year+'/solution.csv'
    dicts_years[year] = create_dict_vars(full_path)
    
result_file = open('results/table_viii_data.csv', 'w')    
result_file.write('dc;type;value\n')


for dc in DCs:
    mape_pv = 0
    mape_bat = 0
  
    forecast_value_bat = round(dicts_years[base_year]["Bat_"+ dc],2)
    forecast_value_pv = round(dicts_years[base_year]["A_"+ dc],2)
        
    for year in years:
        
        real_value_pv = round(dicts_years[year]["A_"+ dc],2)
        if real_value_pv != 0:
          mape_pv += 100 * (1/len(years))  * ( abs( real_value_pv - forecast_value_pv)/ real_value_pv ) 
        
        real_value_bat = round(dicts_years[year]["Bat_"+ dc],2)
        
        if dicts_years[year]["Bat_"+ dc] >1:
            mape_bat += 100 * (1/len(years))  * ( abs( real_value_bat - forecast_value_bat)/ real_value_bat) 

    result_file.write(f'{dc};battery;{round(mape_bat,2)}\n')
    result_file.write(f'{dc};pv;{round(mape_pv,2)}\n')    



result_file.close()
