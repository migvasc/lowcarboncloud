#!/bin/bash
INPUT_FILES="2018.json 2019.json 2020.json 2021.json"

rm -rf results
mkdir results

## Running the experiments

echo '===================================================='
echo 'Running the experiments...'
echo '===================================================='

for input in $INPUT_FILES
do
    python3 scripts/low_carbon_cloud.py --input_file "input/$input" 
done

## Scenario using only power from the grid
python3 scripts/low_carbon_cloud.py --input_file "input/only_grid.json" 

## Scenario using only power from the PV and batteries
python3 scripts/low_carbon_cloud.py --input_file "input/only_pv_bat.json" 

echo '===================================================='
echo 'Extracting the data from the experiments...'
echo '===================================================='

## Extracting the data for each experiment
for input in $INPUT_FILES
do
    python3 scripts/extract_data_figures.py --input_file "input/$input" 
done

python3 scripts/extract_data_figures.py --input_file "input/only_grid.json" 
python3 scripts/extract_data_figures.py --input_file "input/only_pv_bat.json" 

## Extracting data from multiple experiments
python3 scripts/extract_table_v_data.py
python3 scripts/extract_table_vi_vii_data.py
python3 scripts/extract_table_viii_data.py
