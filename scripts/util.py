import json

class Util:
    @staticmethod
    def load_workload(path):
        """ Load the workload that will be processed by the cloud federation

        Parameters
        ----------
        path : str
            The path of the csv file that contains the workload.  

        Returns
        -------
        list
            A list that contains the total number of CPU cores demand per time slot
        """

        workload_array = [0]
        with open(path, encoding="UTF-8") as workload_file:    
            cursor = 0
            for line_number, line in enumerate(workload_file):
                row = line.split(',')
                ncores = int(row[1].replace('\n',''))
                workload_array.append(ncores)
        return workload_array

    @staticmethod
    def load_irradiance(path):    
        """ Load the solar irradiation data for each DC

        Parameters
        ----------
        path : str
            The path of the csv file that contains the irradiation data.  

        Returns
        -------
        list
            A list that contains the solar irradiation (W/m²) per time slot
        """

        irradiation_array = [0]
        with open(path, encoding="UTF-8") as irradiation_file:    
            cursor = 0
            for line_number, line in enumerate(irradiation_file):            
                if (line_number > 0):                    
                    row = line.split(';')
                    solar_irradiation = float(row[1].replace('\n',''))
                    irradiation_array.append(solar_irradiation)

        return irradiation_array
    


    
    @staticmethod
    def validate_json_input(json_data):        
        if not 'DCs' in json_data:
            raise RuntimeError('Necessary to pass the DCs list in the input!')

        if len(json_data['DCs']) == 0:
            raise RuntimeError('Necessary to pass the at least one DC in the DC list of the input!')

        if not 'pIdleDC' in json_data:
            raise RuntimeError('Necessary to pass the pIdleDC of the DCs in the input!')
        
        if not 'PUE' in json_data:
            raise RuntimeError('Necessary to pass the PUE of the DCs in the input!')     

        if not 'C' in json_data:
            raise RuntimeError('Necessary to pass the parameter C of the DCs in the input!')

        if not 'pNetIntraDC' in json_data:
            raise RuntimeError('Necessary to pass the parameter pNetIntraDC of the DCs in the input!')

        if not 'length_k' in json_data:
            raise RuntimeError('Necessary to pass the parameter length_k in the input!')
                    
        if not 'workload_file' in json_data:
            raise RuntimeError('Necessary to pass the parameter workload_file in the input!')

        if not 'workload_file' in json_data:
            raise RuntimeError('Necessary to pass the parameter workload_file in the input!')
       
        if not 'solar_irradiance_dc_file_path' in json_data:
            raise RuntimeError('Necessary to pass the parameter solar_irradiance_dc_file_path of the DCs in the input!')
       

        if not 'grid_co2' in json_data:
            raise RuntimeError('Necessary to pass the parameter grid_co2 of the DCs in the input!')
       

        if not 'pv_co2' in json_data:
            raise RuntimeError('Necessary to pass the parameter pv_co2 of the DCs in the input!')
       
 
        if not 'bat_co2' in json_data:
            raise RuntimeError('Necessary to pass the parameter bat_co2 in the input!')

        if not 'eta_ch' in json_data:
            raise RuntimeError('Necessary to pass the parameter eta_ch in the input!')

        if not 'eta_dch' in json_data:
            raise RuntimeError('Necessary to pass the parameter eta_dch in the input!')

        if not 'eta_pv' in json_data:
            raise RuntimeError('Necessary to pass the parameter eta_pv in the input!')


        if not 'pCore' in json_data:
            raise RuntimeError('Necessary to pass the parameter pCore in the input!')

        for dc in json_data['DCs']:
            if dc not in json_data['pIdleDC']:
                raise RuntimeError('All DCs must have a pIdle value!')
            if dc not in json_data['PUE']:
                raise RuntimeError('All DCs must have a PUE value!')
            if dc not in json_data['C']:
                raise RuntimeError('All DCs must have a C value!')
            if dc not in json_data['pNetIntraDC']:
                raise RuntimeError('All DCs must have a pNetIntraDC value!')
            if dc not in json_data['solar_irradiance_dc_file_path']:
                raise RuntimeError('All DCs must have a solar_irradiance_dc_file_path value!')
            if dc not in json_data['grid_co2']:
                raise RuntimeError('All DCs must have a grid_co2 value!')
            if dc not in json_data['pv_co2']:
                raise RuntimeError('All DCs must have a pv_co2 value!')
    

        return True


    @staticmethod
    def extract_dict_variables(prob):
        """ Creates a hash object with the LP's variables and their computed values

        Parameters
        ----------
        prob : an LPProblem object
            The LPProblem object after it has been solved by the solver.

        Returns
        -------
        hash
            a hash that contains as key the variable name and as value the computed
            value of the variable by the LP
        """
        variables = {}
        for v in prob.variables():
            variables[v.name] = v.varValue    
        return variables    

    @staticmethod
    def load_inputs(input_file):
        """ Load the program inputs from a json file

        Parameters
        ----------
        input_file : str
            The path for the .json input file

        Returns
        -------
        hash
            a hash that contains as key the input name and as value the 
            parameter value
        """
        inputs = {}
        with open(input_file, 'r') as f:
            inputs = json.load(f)
        
        if Util.validate_json_input(inputs):
            inputs['timeslots'] = list(range(inputs['length_k'] ))
            inputs['workload']  = Util.load_workload(inputs['workload_file'])   

        
            inputs['irradiance'] ={}
            for dc in inputs['DCs']:
                inputs['irradiance'][dc] = Util.load_irradiance(inputs["solar_irradiance_dc_file_path"][dc])
            return inputs
        
    @staticmethod
    def load_variables(path):
        """ Load the variables of a solved LP into memory

        Parameters
        ----------
        path : str
            The path for the .csv input file

        Returns
        -------
        hash
            a hash that contains as key the variable name and as value the 
            computed LP value
        """
        variables  = {}
        with open(path, encoding="UTF-8") as variables_file:    
            for line_number, line in enumerate(variables_file):
                row = line.split(';')
                key = row[0]
                value = float(row[1].replace('\n',''))
                variables[key] = value
        return variables

