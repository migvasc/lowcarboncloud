def extract_emissions(path):
    """ Export the total emissions values for the scenarios: 
    - only using the grid
    - only using power from PVs and batteries
    - using power from the grid, pv and batteries
    Parameters
    ----------
    path : str
        The path of the input file
    """


    emissions = ''
    with open(path, encoding="UTF-8") as summary_file:    
        cursor = 0
        for line_number, line in enumerate(summary_file):
            if (line_number == 1):
                columns = line.split(';')
                emissions = columns[1]

    return emissions


result_file = open('results/table_v_data.csv', 'w')    
result_file.write('scenario;emissions\n')

result_file.write(f'grid_only;{extract_emissions("results/only_grid/summary_results.csv")}\n')
result_file.write(f'pv_bat_only;{extract_emissions("results/only_pv_bat/summary_results.csv")}\n')
result_file.write(f'pv_bat_grid;{extract_emissions("results/2021/summary_results.csv")}\n')

result_file.close()