INPUT_FILES="2018.json 2019.json 2020.json 2021.json"

echo '===================================================='
echo 'Generating the plots...'
echo '===================================================='
for input in $INPUT_FILES
do
    Rscript scripts/plots.r $input 
done
echo '===================================================='
echo 'Plots generated!'
echo '===================================================='